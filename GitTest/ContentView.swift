//
//  ContentView.swift
//  GitTest
//
//  Created by Владимир Олейников on 11/7/2022.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
            .padding()
    }
    // master first commit
    // main second commit
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
