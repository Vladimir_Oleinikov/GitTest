//
//  GitTestApp.swift
//  GitTest
//
//  Created by Владимир Олейников on 11/7/2022.
//

import SwiftUI

@main
struct GitTestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
